package com.uniamerica.usuarioExemplo;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.uniamerica.usuarioExemplo.model.SexoEnum;
import com.uniamerica.usuarioExemplo.model.Usuario;

import com.uniamerica.usuarioExemplo.service.UsuarioService;
import org.aspectj.lang.annotation.Before;
import org.checkerframework.checker.units.qual.A;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.web.context.WebApplicationContext;

import java.util.Optional;


import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class Integration_test {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    //utilizado para realizar o parse do objeto para json
    private ObjectMapper objectMapper;

    @Autowired
    private UsuarioService usuarioService;


    @Test
    void insereUsuario_Test() throws Exception {

        Usuario usuario = new Usuario();
        usuario.setSexo(SexoEnum.Masculino);
        usuario.setEmail("asdasd@gmail.com");
        usuario.setTelefone("12121212");
        usuario.setNome("user1");

        this.mockMvc.perform(MockMvcRequestBuilders
                .post("/usuario")
                .content(objectMapper.writeValueAsString(usuario))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
    }

    @Test
    void listaTodosUsuario_Test() throws Exception {

        //DESCOMENTAR LINHAS ABAIXO CASO QUERIA RODAR O TESTE INDIVIDUALMENTE
//        Usuario usuario = new Usuario();
//        usuario.setSexo(SexoEnum.Masculino);
//        usuario.setEmail("asdasd@gmail.com");
//        usuario.setTelefone("12121212");
//        usuario.setNome("user1");
//        usuarioService.insereUsuario(usuario);

        this.mockMvc.perform(MockMvcRequestBuilders
        .get("/usuario")
        .accept(MediaType.APPLICATION_JSON))
        .andDo(print())
        .andExpect(status().isOk());
    }


    @Test
    void getUsuarioByNome() throws Exception {

        this.mockMvc.perform(MockMvcRequestBuilders
        .get("/usuario/name/{name}", "user1")
        .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    void deleteUsuario() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders.delete("/usuario/{id}", 1))
                .andExpect(status().isAccepted());
    }

    @Test
    void alteraUsuairo() throws Exception {

        Optional<Usuario> userBd = usuarioService.findById(1);

        userBd.ifPresent(usuario -> usuario.setNome("alterado"));

        this.mockMvc.perform( MockMvcRequestBuilders
        .put("/usuario/{id}",1)
        .content(objectMapper.writeValueAsString(userBd.get()))
        .contentType(MediaType.APPLICATION_JSON)
        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.nome").value("alterado"));
    }









}
